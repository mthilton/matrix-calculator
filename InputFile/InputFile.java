/*
***********************************
* Matthew Hilton                  *
* Feb. 2st, 2018                  *
* Uhhhh please dont steal my code *
***********************************

*******************************************************************************
* This file is to learn how to handle input files for java since I don't know *
* how... yet (UPDATE 2-28-2018: File I/O is handled. Now working on matrix    *
* I/O)                                                                        *
*******************************************************************************
*/
import java.io.*;

class inputFile{
  public static void main(String args[]){

      //Main Variables
      String inputfilename = "input.txt";
      String outputfilename = "output.txt";
      String line = null;

      try{
        FileReader fileReader = new FileReader(inputfilename);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        FileWriter fileWriter = new FileWriter(outputfilename);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

        while((line = bufferedReader.readLine()) != null){
          char[] inputLine = line.toCharArray();                                //Turns the String input into an array of chars
          String inputInt = "";                                                 //Initializes the String Array that will store the individual numbers

          for(int i=0;i<inputLine.length;i++){
            if(inputLine[i] == ';' || inputLine[i] == ')'){                     //Checks to see if the current char is a ';' or a ')'. These are the conditions in which we know that our number string has ended
              int initVal = Integer.parseInt(inputInt);                         //Parses the String Number to an int value called initVal
              String finalVal = Integer.toString(initVal+5);                    //Does math on initVal then returns it as a string
              bufferedWriter.write(finalVal);                                   //Writes the final String val to output file
              bufferedWriter.write(inputLine[i]);                               //Writes the ';' or ')' to the output file for clarity
              inputInt = "";                                                    //Reinitalizes the String number to be used over again
            } else if(inputLine[i] == '('){                                     //Checks to see if the current char is a '('
              bufferedWriter.write(inputLine[i]);                               //If it is then we just write it back to the file. This signifies the start to a row
            } else {
              inputInt += inputLine[i];                                         //Adds the current char to the string number. This is so we can handle any sized diget number
            }
          }
          bufferedWriter.newLine();
        }
        bufferedReader.close();
        bufferedWriter.close();
      }

      //Error Reporting
      catch(FileNotFoundException ex){
        System.out.println("Unable to open file '" + inputfilename + "'");
      }

      catch(IOException ex){
        System.out.println("Error reading or writing. Here is the Stack trace:");
        ex.printStackTrace();
      }
    }
}
