#Matrix Calculator
Author: Matthew Hilton

People that assisted: Jessy Armstrong, Adam Ames, Daniel Wright

--------------------------------------------------------------------------

##How to run it

1. Make sure to update input.txt to include the two matrices (or one
in the cases of Scale, Invert, Determinate, and Row Reduce) that you would like to
use.


2. Type the following in your command line (NOTE: you must be in the
directory that the jar file is in):

```
./matrixCalculator.jar <operation> <input_file> <scalar>
```

3. Check the output file to get your answer

--------------------------------------------------------------------------
##Valid Operations:

* Add
* Subtract
* Scale
* Mult (Multiply)
* Det (Determinate)
* Invert
* REF (Row Reduce)

--------------------------------------------------------------------------

##Known Bugs:

- Only works with whole numbers
    - ***Solution:*** currently uses the *int* primitive datatype, going to change it to *double* once I have built everything.

--------------------------------------------------------------------------

##Upcoming Features:

- [X] Adding and subtracting matrices
- [X] Scaling
- [X] Multiplying matrices
- [ ] Determinate of a matrix
- [ ] Inverting a matrix
- [ ] Gaussian elimination

--------------------------------------------------------------------------

##Changelog:

12-9-2018

- Finished mult
- Reorganized code
- wrote a helper function for writing/printing results
- Updated README

11-9-2018

- After a hiatus (and actually learning java) I have returned. Here is what changed:
    - Cleaned up the code part, got rid of a TON of unnecessary lines
    - Removed direct access to the elements of the graph ADT, now there are functions for
      any accesses or manipulations I might need.
    - Realized there isn't such thing as Dotting or Crossing matrices, was confused with
      vectors. Changed those ops to Scaling and Multiplying
    - Updated README

16-3-2018

- Built Dotting
    - ***Note:*** need to test more edge cases. I only tried one 2x2 matrix
- Updated README

13-3-2018

- Built Subtraction
- Made some nice changes to the README

9-3-2018

- Created the Weighted Graph ADT
- Created the I/O Methodology
- Built Addition
