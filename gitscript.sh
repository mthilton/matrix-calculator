#!/bin/bash

################################################
#                                              #
# Matthew Hilton                               #
# May 4, 2018                                  #
# mthilton@ucsc.edu                            #
# Uhh ask to use this code?? ty                #
#----------------------------------------------#
# This is a simple script that adds the files  #
# from my working repository to the gitlab     #
# repository.                                  #
################################################


clear 

if [ $# -gt 1 ]; then
	echo usage: $0 [source] \n Leave blank to commit the entire repository.
	exit 1
fi

source=$1

make clean

git add ./$source && \

echo "Enter your commit message:"

read message


git commit -m "$message" && \
git push
