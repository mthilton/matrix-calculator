MAINCLASS  = matrixCalculator
JARFILE    = $(MAINCLASS).jar
JAVASRC    = $(wildcard *.java)
CLASSES    = $(patsubst %.java, %.class, $(JAVASRC))

all: $(JARFILE)

$(JARFILE): $(CLASSES)
	echo Main-class: $(MAINCLASS) > Manifest
	jar cvfm $(JARFILE) Manifest $(CLASSES)
	rm Manifest
	chmod +x $(JARFILE)

%.class: %.java
	javac -Xlint $<

clean:
	rm $(CLASSES) $(JARFILE) output.txt
