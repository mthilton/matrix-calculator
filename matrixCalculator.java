/*
***********************************
* Matthew Hilton                  *
* Feb. 1st, 2018                  *
* Uhhhh please dont steal my code *
***********************************
*/
import java.io.*;

public class matrixCalculator{

    public static WeightedGraph finalMatrixA = null;
    public static WeightedGraph finalMatrixB = null;
    public static WeightedGraph finalMatrixC = null;
    public static int scalar = 0;

    public static void main(String args[]){

        // Checks Cmd Line args to ensure proper formatting
        String operation = args[0].toLowerCase();

        // if only one arg and it equals 'help' print list of avalible ops
        if(args.length == 1){
            if(operation.equals("help")) {
                System.out.println("Usage: ./matrixCalculator.jar \"operation\" <input_file>\n");
                System.out.println("Availible operations:\nAdd\nSubtract\nScale\nMult (Multiply)\n\nComming Soon:\nDet (Determinate)\nInvert\nREF (Row Reduce)");
                System.exit(1);
            }
        }

        // If 3 args check 1st arg == scale and check 3 arg is valid int "Will swap to floats eventually"
        if(args.length == 3){
            if(operation.equals("scale")){
                try{
                    scalar = Integer.parseInt(args[2]);
                } catch (NumberFormatException e){
                    System.err.println("matrixCalculator.jar scale <input_file> <scalar>");
                    System.err.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~^^^^^^");
                    System.err.println("Is not a valid integer");
                    System.exit(1);
                }
            } else {
                System.err.println("Usage: ./matrixCalculator.jar scale <input_file> <scalar>\n");
                System.exit(1);
            }
        }

        // If it is not either of the two special cases and it is not the default case of 2 args, print err msg
        if(args.length > 3 || args.length < 1){
            System.err.println("Usage: ./matrixCalculator.jar \"operation\" <input_file>");
            System.exit(1);
        }

        String inputfilename = args[1];
        String outputfilename = "output.txt";

        try{
            BufferedReader reader = new BufferedReader(new FileReader(inputfilename));
            BufferedWriter writer = new BufferedWriter(new FileWriter(outputfilename));
            Boolean singleMatrix = false;

            // Check to make sure that we have valid file references
            if(reader == null) throw new IllegalArgumentException("Reader is Null!");
            if(writer ==  null) throw new IllegalArgumentException("Writer is Null!");

            // Check to see if we should look for 1 matrix or 2
            if (operation == "scale" || operation == "det" || operation == "ref" || operation == "invert") { singleMatrix = true; }

            // Grabs the necessay matricies from the input file.
            finalMatrixA = getMatrix(reader);
            if(!singleMatrix) { finalMatrixB = getMatrix(reader); }

            // Verifies that all Req'd Matricies were found and created
            if(finalMatrixA == null) {
                System.err.println("Error Getting Matrix A!");
                System.exit(1);
            }

            if(finalMatrixB == null && !singleMatrix ) {
                System.err.println("Error Getting Matrix B!");
                System.exit(1);
            }

            // Decieds what op to do
            switch(operation){

                case "add":
                    finalMatrixC = addMatricies(finalMatrixA, finalMatrixB);
                    break;

                case "subtract":
                    finalMatrixC = subtractMatricies(finalMatrixA, finalMatrixB);
                    break;

                case "scale":
                    finalMatrixC = scaleMatrix(finalMatrixA, scalar);
                    break;

                case "mult":
                    finalMatrixC = multMatricies(finalMatrixA, finalMatrixB);
                    break;

                case "det":
                    writer.write("Not yet");
                    break;

                case "invert":
                    writer.write("Not yet");
                    break;

                case "ref":
                    writer.write("Not yet");
                    break;

                default:
                    System.out.println("Error: Invalid Operation Argument");
            }

            results(finalMatrixC, operation, writer);

            reader.close();
            writer.close();
        }

        // Error Reporting
        catch(FileNotFoundException ex){
            System.out.println("Unable to open file '" + inputfilename + "'");
        }

        catch(IOException ex){
            System.out.println("Error reading or writing. Here is the Stack trace:");
            ex.printStackTrace();
        }
    }

    // Functions ----------------------------------------------------------------------------

    // This is to find the dimentions that the matrix is suppose to be
    static int getMatrixDim(String inputLine){

        System.out.print("  Retrieving Matrix dimensions... ");

        // Turns the string into an array of strings
        // Each index is one item in the first row of the matrix
        String[] tokens = inputLine.split("[(;)]+");

        System.out.println("Done!");
        return tokens.length - 1;
    }

    // Creates a matrix initialized to 0 that is the size of the matrix in the input file
    static WeightedGraph createEmptyMatrix(int size){

        System.out.print("Creating matrix... ");
        WeightedGraph matrix = new WeightedGraph(size);
        matrix.createMatrixIndeces();
        matrix.initializeMatrix();
        System.out.println("Done!");
        return matrix;
    }

    // Fills a matrix with the contents from the input file
    static void fillMatrix(WeightedGraph matrix, BufferedReader reader, String line, int matrixDim) throws java.io.IOException {

        // Fucntion Vars
        int matrixIndexJ = 0;   // row
        int matrixIndexI = 0;   // column
        String[] newInputLine = line.split("[(;)]+");

        System.out.print("Filling Matrix with desired values... ");

        // While we are still on the matrix,
        for(int z = 0; z < matrixDim; z++){

            // Update the matrix with the proper weight
            for(int i = 1; i < newInputLine.length; i++){

                // Parses the String Number to an int value called weight
                int weight = Integer.parseInt(newInputLine[i]);

                // Adds the value from the input file to the matrix obj at the correct index
                matrix.addEdge(matrixIndexI, matrixIndexJ, weight);
                matrixIndexJ++;

                // If we fell of the row array, reset it to 0 and increment the column number
                if(matrixIndexJ == matrix.matrixSize()){
                    matrixIndexI++;
                    matrixIndexJ = 0;
                }

            }

        // Moves to the next line and splits it into an array like before.
        line = reader.readLine();
        newInputLine = line.split("[(;)]+");

        }
        System.out.println("Done!");
    }

    // Creates and fills a new matrix object with the proper values
    static WeightedGraph getMatrix(BufferedReader reader) throws java.io.IOException {

        // Fucntion Vars
        String line = reader.readLine();
        String lineFinal = "";

        // Finds the place where the matrix starts
        System.out.print("Finding Matrix... ");
        while((line = reader.readLine()) != null){
            if(line.indexOf('(') > -1){
                System.out.println("Done!");
                lineFinal = line;
                break;
            } else {
                continue;
            }
        }

        // Creates and fills the matrix object with the proper size and values
        WeightedGraph newMatrix = createEmptyMatrix(getMatrixDim(lineFinal));
        int size = newMatrix.matrixSize();
        fillMatrix(newMatrix, reader, lineFinal, size);
        return newMatrix;
    }

    // Prints out the proper results
    static void results(WeightedGraph result, String op, BufferedWriter writer) throws IOException {
        if( result == null ) {
            System.err.println(op + "failed!");
            return;
        }

        System.out.println("This is the resultant matrix:");
        writer.write("This is the resultant matrix:\n");
        result.printMatrix(writer);
        System.out.println("This Matrix can be found in the file \"output.txt\"");
    }

    // Adds the two matricies that were passed together and returns a new matrix
    static WeightedGraph addMatricies(WeightedGraph A, WeightedGraph B) {

        // Checking for valid objects
        if( A == null ){
            System.err.println("A is equal to null!");
            return null;
        }

        if ( B == null ){
            System.err.println("B is equal to null!");
            return null;
        }

        // Function vars
        WeightedGraph newMatrix = null;

        if(A.matrixSize() != B.matrixSize()){
            System.err.println("\nError: Matrices are not the same size. Use matricies that have the same dimensions.");
            System.exit(1);
        } else {
            System.out.println("~~~~~~~~~~~~~~~~~~~~~");
            System.out.println("Adding Matricies... ");
            System.out.println("~~~~~~~~~~~~~~~~~~~~~");

            // Set up for adding the two original matrices
            newMatrix = createEmptyMatrix(A.matrixSize());
            int weight = 0;
            Boolean errFlagA = false;
            Boolean errFlagB = false;
            System.out.print("Adding Matricies A and B... ");

            // for each index in the matrix, get the weight at each index, add them together
            // and store them in the new matrix
            for(int j=0; j<newMatrix.matrixSize(); j++) {
                for(int i=0; i<newMatrix.matrixSize(); i++) {
                    weight = (A.getEdge(j, i, errFlagA) + B.getEdge(j, i, errFlagB));

                    if(errFlagA) {
                        System.err.println("addMatricies, ln: 276: Unable to get edge [" + j + "][" + i + "] in Matrix A!");
                        return null;
                    }

                    if(errFlagB) {
                        System.err.println("addMatricies, ln: 276: Unable to get edge [" + j + "][" + i + "] in Matrix B!");
                        return null;
                    }

                    newMatrix.addEdge(i, j, weight);
                }
            }

        }
        System.out.println("Done!");
        return newMatrix;
    }

    // Subtracts the two matricies that were passed together and returns a new matrix
    static WeightedGraph subtractMatricies(WeightedGraph A, WeightedGraph B) {

        // Checking for valid objects
        if( A == null ){
            System.err.println("A is equal to null!");
            return null;
        }

        if ( B == null ){
            System.err.println("B is equal to null!");
            return null;
        }

        // Function vars
        WeightedGraph newMatrix = null;

        if(A.matrixSize() != B.matrixSize()){
            System.err.println("\nError: Matrices are not the same size. Use matricies that have the same dimensions.");
            System.exit(1);
        } else {
            System.out.println("~~~~~~~~~~~~~~~~~~~~~");
            System.out.println("Subtracting Matricies... ");
            System.out.println("~~~~~~~~~~~~~~~~~~~~~");

            // Set up for adding the two original matrices
            newMatrix = createEmptyMatrix(A.matrixSize());
            int weight = 0;
            Boolean errFlagA = false;
            Boolean errFlagB = false;
            System.out.print("Subtracting Matricies A and B... ");

            // for each index in the matrix, get the weight at each index, subtract them
            // and store them in the new matrix
            for(int j=0; j<newMatrix.matrixSize(); j++) {
                for(int i=0; i<newMatrix.matrixSize(); i++) {
                    weight = (A.getEdge(j, i, errFlagA) - B.getEdge(j, i, errFlagB));

                    if(errFlagA) {
                        System.err.println("subtractMatricies, ln: 333: Unable to get edge [" + j + "][" + i + "] in Matrix A!");
                        return null;
                    }

                    if(errFlagB) {
                        System.err.println("subtractMatricies, ln: 333: Unable to get edge [" + j + "][" + i + "] in Matrix B!");
                        return null;
                    }

                    newMatrix.addEdge(i, j, weight);
                }
            }

        }
        System.out.println("Done!");
        return newMatrix;
    }

    // Dots two matrices and returns the dot product
    static WeightedGraph scaleMatrix(WeightedGraph A, int s) {

        // Checking for valid objects
        if( A == null ){
            System.err.println("A is equal to null!");
            return null;
        }

        // Function Vars
        Boolean errFlag = false;
        int newVal = 0;
        WeightedGraph newMatrix = createEmptyMatrix(A.matrixSize());

        System.out.println("~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Scaling Matrix... ");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~");

        // For every index in each matrix, dot them together then add them to the running sum
        for(int j = 0; j < A.matrixSize(); j++){
            for(int i = 0; i < A.matrixSize(); i++){
                newVal = A.getEdge(j, i, errFlag) * s;

                if(errFlag) {
                    System.err.println("scaleMatrix, ln: 383: Unable to get edge [" + j + "][" + i + "] in Matrix A!");
                    return null;
                }

                newMatrix.addEdge(j, i , newVal);
            }
        }
        return newMatrix;
    }

    // Multiplies the two matrices
    static WeightedGraph multMatricies(WeightedGraph A, WeightedGraph B){

        // Checking for valid objects
        if( A == null ){
            System.err.println("A is equal to null!");
            return null;
        }

        if ( B == null ){
            System.err.println("B is equal to null!");
            return null;
        }

        // Function Vars
        int k = 0, l = 0, m = 0, n = 0, val = 0;                    // Matrix Indices
        WeightedGraph newMatrix = createEmptyMatrix(A.matrixSize());     // New Matrix
        Boolean errFlagA = false;
        Boolean errFlagB = false;

        /*
        Matrix Insices - Explained:
            row   column
              j   i       - new matrix
              l   k       - matrix a
              n   m       - matrix b
        */

        System.out.println("~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Multiplying Matricies... ");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~");

        // Each index of the new array is the sum all of the elements in a given row of matrix a scaled by the respective
        // coulumn values in matrix b
        for(int j = 0; j < newMatrix.matrixSize(); j++){
            for(int i = 0; i < newMatrix.matrixSize(); i++){
                while(k < A.matrixSize() && n < B.matrixSize()){
                    val += (A.getEdge(k, l, errFlagA) * B.getEdge(m, n, errFlagB));

                    if(errFlagA) {
                        System.err.println("multMatricies, ln: 435: Unable to get edge [" + j + "][" + i + "] in Matrix A!");
                        return null;
                    }

                    if(errFlagB) {
                        System.err.println("multMatricies, ln: 435: Unable to get edge [" + j + "][" + i + "] in Matrix B!");
                        return null;
                    }

                    k++;
                    n++;
                }
                newMatrix.addEdge(j, i, val);
                val = 0;
                k = 0;
                n = 0;
                m++;
            }
            m = 0;
            l++;
        }
        return newMatrix;
    }


}
