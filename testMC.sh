clear
make clean
make
echo " "

if (( $# == 1 ))
then
    FILE=$1
else
    FILE=input.txt
fi

./matrixCalculator.jar help
paste $FILE
./matrixCalculator.jar add $FILE
./matrixCalculator.jar subtract $FILE
./matrixCalculator.jar scale $FILE 20
./matrixCalculator.jar mult $FILE
