/*
***********************************
* Matthew Hilton                  *
* Feb. 17th, 2018                 *
* Uhhhh please dont steal my code *
***********************************

************************************************************************************
* This is the graph file. It will contain the structure for the Graph ADT that I   *
* hope to implemnt. This Graph will be a directed weighted graph. I need to figure *
* out how to create an unweighted graph first. I anticipate that a weighted Graph  *
* will be the same however the addEdge function will need to be passed a weight    *
* Value.                                                                           *
************************************************************************************

***********************************************************************
* Sources that helped me build this program:                          *
* http:// cs.fit.edu/~ryan/java/programs/graph/WeightedGraph-java.html *
***********************************************************************
*/
import java.io.*;

public class WeightedGraph{

    // Class Vars
    public int [][] edges;    // This is the actual matrix. This is a 2d array and it will store the weight of the connected verticies
    public Object [] labels;  // This is an array of all the verticies.

    // Constructor
    public WeightedGraph(int n){

        edges  = new int [n][n];    // This is the 2d array of weights given the number of verticies. All matricies are nxn
        labels = new Object[n];     // This is a list of all the verticies

    }

// These methods I directly copied from the listed src ---------------------------------

    // This returns the size of the matrix
    public int matrixSize(){
        return labels.length;
    }

    // finds a given vertex in the array of vertecies
    public Object getLabel(int vertex){
        return labels[vertex];
    }

    // adds an edge with a weight to a given two vertexes
    public void addEdge(int srcVertex, int dstVertex, int w){
        edges[srcVertex][dstVertex] = w;
    }

    // finds and prints all of the neighbors a given vertex has
    public int [] neighbors(int vertex){

        // Method Var
        int numberOfNeighbors = 0;

        // this loop is to count how many neighbors that the given vertex has. This is so we can get the size of the array we need
        for(int i = 0; i < labels.length; i++){
            if (edges[vertex][i] > 0){
                numberOfNeighbors++;
            }
        }

        final int neighbors[] = new int[numberOfNeighbors];
        numberOfNeighbors = 0;

        // this loop creates a new array, finds and finds all of the neghbors of a given vertex.
        for(int i = 0; i < labels.length; i++){
            if (edges[vertex][i] > 0){
                neighbors[numberOfNeighbors++] = i;
            }
        }
        return neighbors;
    }

    public void printMatrix(BufferedWriter out) throws IOException {
        for(int j=0; j<edges.length; j++) {
            System.out.print(labels[j]+": ");
            out.write(labels[j]+": ");
            for(int i=0; i<edges[j].length; i++) {
                System.out.print(+edges[j][i]+" ");
                out.write(+edges[j][i]+" ");
            }
            System.out.println();
            out.write("\n");
        }
        System.out.println("~~~~~~~~~~~~~~~~~~~~");
        out.write("\n~~~~~~~~~~~~~~~~~~~~");
    }


    // These methods I wrote on my own

    // This sets the label for each row/column
    public void createMatrixIndeces(){
        for(int newVertex = 0; newVertex < labels.length; newVertex++){
            labels[newVertex] = newVertex;
        }
    }

    // boolean check to see if two verteces have an edge between them
    public Boolean isEdge(int srcVertex, int dstVertex){
        if(edges[srcVertex][dstVertex] > 0){
            return true;
        } else {
            return false;
        }
    }

    public int getEdge(int r, int c, Boolean errFlag){

        // Error checking:
        if(r > this.edges.length - 1){
            errFlag = true;
            System.err.println("r is too big!");
            return 0;
        }

        if(c > this.edges.length - 1){
            System.err.println("c is too big!");
            errFlag = true;
            return 0;
        }

        errFlag = false;
        return this.edges[r][c];
    }

    // removes an edge from a given matrix
    public void removeEdge(int srcVertex, int dstVertex){
        if(isEdge(srcVertex, dstVertex) == false){
            return;
        } else {
            edges[srcVertex][dstVertex] = 0;
        }
    }

    // Sets all values in matrix to 0
    public void initializeMatrix(){
        for(int i = 0; i < labels.length; i++){
            for(int j = 0; j <labels.length; j++){
                addEdge(i, j, 0);
            }
        }
    }
}
