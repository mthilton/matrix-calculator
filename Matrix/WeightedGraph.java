/*
***********************************
* Matthew Hilton                  *
* Feb. 17th, 2018                 *
* Uhhhh please dont steal my code *
***********************************

************************************************************************************
* This is the graph file. It will contain the structure for the Graph ADT that I   *
* hope to implemnt. This Graph will be a directed weighted graph. I need to figure *
* out how to create an unweighted graph first. I anticipate that a weighted Graph  *
* will be the same however the addEdge function will need to be passed a weight    *
* Value.                                                                           *
************************************************************************************

***********************************************************************
* Sources that helped me build this program:                          *
* http://cs.fit.edu/~ryan/java/programs/graph/WeightedGraph-java.html *
***********************************************************************
*/

public class WeightedGraph{

  //Class Vars
  public int [][] edges;                                                        //This is the actual matrix. This is a 2d array and it will store the weight of the connected verticies
  public Object [] labels;                                                      //This is an array of all the verticies.
  //Constructor
  public WeightedGraph(int n){

    edges  = new int [n][n];                                                    //This is the 2d array of weights given the number of verticies. All matricies are nxn
    labels = new Object[n];                                                     //This is a list of all the verticies

  }

//These methods I directly copied from the listed src
  public int matrixSize(){                                                      //This returns the size of the matrix
    return labels.length;
  }

  public Object getLabel(int vertex){                                           //finds a given vertex in the array of vertecies
    return labels[vertex];
  }


  public void addEdge(int srcVertex, int dstVertex, int w){                     //adds an edge with a weight to a given two vertexes
    edges[srcVertex][dstVertex] = w;
  }

  public int [] neighbors(int vertex){                                          //finds and prints all of the neighbors a given vertex has
    int numberOfNeighbors = 0;
    for(int i = 0; i < labels.length; i++){                                     //this loop is to count how many neighbors that the given vertex has. This is so we can get the size of the array we need
      if (edges[vertex][i] > 0){
        numberOfNeighbors++;
      }
    }
    final int neighbors[] = new int[numberOfNeighbors];
    numberOfNeighbors = 0;
    for(int i = 0; i < labels.length; i++){                                     //this loop creates a new array, finds and finds all of the neghbors of a given vertex.
      if (edges[vertex][i] > 0){
        neighbors[numberOfNeighbors++] = i;
      }
    }
    return neighbors;
  }

  public void printMatrix() {
     for(int j=0; j<edges.length; j++) {
        System.out.print (labels[j]+": ");
        for(int i=0; i<edges[j].length; i++) {
          System.out.print(+edges[j][i]+" ");
        }
        System.out.println();
     }
     System.out.println("~~~~~~~~~~~~~~~~~~~~");
  }


//These methods I wrote on my own

  public void createMatrixIndeces(){                                            //This sets the label for each row/column
    for(int newVertex = 0; newVertex < labels.length; newVertex++){
      labels[newVertex] = newVertex;
    }
  }

  public Boolean isEdge(int srcVertex, int dstVertex){                          //boolean check to see if two verteces have an edge between them
    if(edges[srcVertex][dstVertex] > 0){
      return true;
    } else {
      return false;
    }
  }

  public void removeEdge(int srcVertex, int dstVertex){                         //removes an edge from a given matrix
    if(isEdge(srcVertex, dstVertex) == false){
      return;
    } else {
      edges[srcVertex][dstVertex] = 0;
    }
  }

  public void initializeMatrix(){                                               //Sets all values in matrix to 0
    for(int i = 0; i < labels.length; i++){
      for(int j = 0; j <labels.length; j++){
        addEdge(i, j, 0);
      }
    }
  }

}
