/*
***********************************
* Matthew Hilton                  *
* Mar. 6th, 2018                  *
* Uhhhh please dont steal my code *
***********************************

******************************************************************
* This file is to figure out how to create a new object and have *
* it be used throught the class                                  *
******************************************************************
*/
import java.io.*;


public class matrix{

  public static WeightedGraph finalMatrixA = null;


  public static void main(String args[]){

    //String operation = args[2];
    String inputfilename = "input.txt";
    String outputfilename = "output.txt";


    try{
      BufferedReader reader = new BufferedReader(new FileReader(inputfilename));
      BufferedWriter writer = new BufferedWriter(new FileWriter(outputfilename));

      finalMatrixA = getMatrix(reader);
      System.out.println("\n~~~~~~~~~~~~~~~~~~~~");
      System.out.println("Here is matrix A: ");
      System.out.println("~~~~~~~~~~~~~~~~~~~~");
      finalMatrixA.printMatrix();


      reader.close();
      writer.close();
    }

    catch(FileNotFoundException x){
      System.out.println("Unable to open file '" + inputfilename + "'");
    }

    catch(IOException x){
      System.out.println("Error reading or writing. Here is the Stack trace:");
      x.printStackTrace();
    }

  }

  static int getMatrixDim(char[] inputLine){                                    //This is to find the dimentions that the matrix is suppose to be
    String inputLineString = new String(inputLine);
    System.out.print("  Retrieving Matrix dimensions...");
    int matrixDim = 1;
    for(int i = 0; i<inputLine.length; i++){
      if(inputLine[i] == ';'){
        matrixDim++;
      } else {
        continue;
      }
    }
    System.out.println("Done!");
    return matrixDim;
  }

  static void fillMatrix(WeightedGraph matrix, BufferedReader reader, String line, int matrixDim) throws java.io.IOException {
    //Method Vars
    int matrixIndexJ = 0;                                                       //row
    int matrixIndexI = 0;                                                       //column
    String stringWeight = "";
    char[] newInputLine = line.toCharArray();
    String newInputLineString = new String(newInputLine);

    System.out.print("  Filling Matrix with desired values...");
    for(int z = 0; z < matrixDim; z++){                                         //Ensures that we only look at the lines the matrix is actually on
      for(int i=0;i<newInputLine.length;i++){
        if(newInputLine[i] == ';' || newInputLine[i] == ')'){                   //Checks to see if the current char is a ';' or a ')'. These are the conditions in which we know that our number string has ended
          int weight = Integer.parseInt(stringWeight);                          //Parses the String Number to an int value called weight
          matrix.addEdge(matrixIndexI, matrixIndexJ, weight);                   //Adds the value from the input file to the matrix obj at the correct index
          stringWeight = "";
          matrixIndexJ++;
          if(matrixIndexJ == matrix.labels.length){
            matrixIndexI++;
            matrixIndexJ = 0;
          }

        } else if(newInputLine[i] == '('){                                      //Checks to see if the current char is a '('
          continue;                                                             //If it is then we just write it back to the file. This signifies the start to a row
        } else {
          stringWeight += newInputLine[i];                                      //Adds the current char to the string number. This is so we can handle any sized diget number
        }
      }
      line = reader.readLine();
      newInputLine = line.toCharArray();
    }
    System.out.println("Done!");
  }

  //Creates and fills a new matrix object with the proper values
  static WeightedGraph getMatrix(BufferedReader reader) throws java.io.IOException {
    //Method Vars
    String line = reader.readLine();
    String lineFinal = "";

    System.out.print("Finding Matrix...");
    while((line = reader.readLine()) != null){
      if(line.indexOf('(') > -1){
        System.out.println("Done!");
        lineFinal = line;
        break;
      } else {
        continue;
      }
    }
    System.out.println("Creating matrix...");
    char[] inputLine = lineFinal.toCharArray();
    int n = getMatrixDim(inputLine);
    WeightedGraph Matrix = new WeightedGraph(n);                                //creates a new matrix object called MatrixA
    Matrix.createMatrixIndeces();                                               //Creates the indecies for the new matrix.
    Matrix.initializeMatrix();
    fillMatrix(Matrix, reader, line, n);
    System.out.println("Done!");
    return Matrix;
  }
}
